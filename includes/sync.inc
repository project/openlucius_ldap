<?php
/**
 * @file
 * This file contains the methods for the sync.
 */

/**
 * Custom batch operation for syncing user settings.
 */
function _openlucius_ldap_sync_batch() {
  // Initiate vars.
  $i = 0;

  // Get dataset to work on (select all user except super-admin).
  $dataset = db_query('SELECT uid FROM {users} WHERE uid <> 1 AND uid <> 0')->fetchAll();

  // Split list into multiple arrays.
  $chunks = array_chunk($dataset, 50);

  // For storing operations.
  $operations = array();

  // Count chunks.
  $count = count($chunks);

  // Assign functions to chunk.
  foreach ($chunks as $chunk) {
    $i++;

    $operations[] = array(
      "openlucius_ldap_sync_process_users",
      array(
        $chunk,
        'details' => t('(Importing chunk @chunk of @count)', array(
            '@chunk' => $i,
            '@count' => $count,
          )),
      ),
    );
  }

  // Put all that information into our batch array.
  $batch = array(
    'operations'    => $operations,
    'title'         => t('User Sync'),
    'init_message'  => t('Initializing'),
    'error_message' => t('An error occurred'),
    'finished'      => 'openlucius_ldap_sync_finished',
  );

  // Get the batch process all ready.
  batch_set($batch);
  $batch =& batch_get();

  // Because we are doing this on the back-end, we set progressive to false.
  $batch['progressive'] = FALSE;

  //Start processing the batch operations.
  drush_backend_batch_process();
}

/**
 * Function for syncing user data.
 *
 * @param array $chunk
 *   The user chunk to be processed.
 * @param $operation_details
 * @param $context
 */
function openlucius_ldap_sync_process_users(array $chunk, $operation_details, &$context) {

  // Get sid from LDAP server.
  $sid = _openlucius_ldap_get_server();

  // Check if we have a server.
  if ($sid != FALSE) {

    // Loop through chunks.
    foreach ($chunk as $uid) {

      // Load user.
      $account = user_load($uid->uid);

      // Notify the user that we something is happening.
      print dt("!name is being synced \n", array('!name' => $account->name));

      // Connect to LDAP.
      $ldap = new LdapServer($sid);
      $ldap->connect();
      $ldap->bind();

      // The default UAT enabled user query.
      $user_query = "(&(samaccountname=" . $account->name . ")(!(userAccountControl:1.2.840.113556.1.4.803:=2)))";

      // Allow other modules to alter the user query.
      drupal_alter('openlucius_ldap_user_uac_search_query', $user_query, $account);

      // Search the current user.
      $tmp_result = $ldap->search('', $user_query);

      // Check if we have results.
      if ($tmp_result['count'] > 0) {

        // Check if account is disabled, if it is enable it.
        if ($account->status == 0) {
          $account->status = 1;
          user_save($account);
        }

        // Check if a module uses our function.
        if (count(module_implements('openlucius_ldap_user_sync')) > 0) {

          // Call modules that implement the hook, and let them change $vars.
          $account = module_invoke_all('openlucius_ldap_user_sync', $account, $tmp_result);
          if (is_array($account) && isset($account[0]) && is_object($account[0])) {
            $account = $account[0];
          }
        }

        // Initialize array for groups sync.
        $groups = array();

        // Check if global mapping is set.
        if (variable_get('openlucius_ldap_global_mapping', '') != '') {

          // Fetch the global mapping.
          $global_mapping = variable_get('openlucius_ldap_global_mapping', '');

          // Explode on comma.
          $global_entries = explode(',', $global_mapping);

          // Check if we have entries.
          if (!empty($global_entries)) {

            // Loop through entries.
            foreach ($global_entries as $entry) {

              // Check if we have a matching entry, if we do append it to groups.
              openlucius_ldap_match_group($entry, $groups);
            }
          }
        }

        // Append the groups defined in the config based on the ldap result.
        openlucius_ldap_map_groups($groups, $tmp_result);

        // Check if we have a group we should be a member of.
        if (!empty($groups)) {

          // Add user to groups.
          _openlucius_ldap_add_user_to_groups($account, $groups);
        }
      }
      // If not it's a disabled account.
      else {

        // The default query for disabled accounts.
        $user_query = "(&(samaccountname=" . $account->name . "))";

        // Allow other modules to alter the user query.
        drupal_alter('openlucius_ldap_user_search_query', $user_query, $account);

        // Search the user.
        $tmp_result = $ldap->search('', $user_query);

        // If we have a result disable the user.
        if ($tmp_result['count'] > 0) {
          $account->status = 0;
          user_save($account);
        }
      }
    }
  }
}
