<?php
/**
 * @file
 * This file contains the forms for the OpenLucius LDAP module.
 */

/**
 * Form constructor for mapping LDAP users to groups.
 * @ingroup forms
 */
function _openlucius_ldap_form($form, &$form_state) {

  $form['global_mapping_ajax'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Global mapping form'),
  );

  $form['global_mapping_ajax']['openlucius_ldap_global_mapping'] = array(
    '#title'             => t('Global mapping'),
    '#type'              => 'textfield',
    '#autocomplete_path' => 'autocomplete/groupname',
    '#description'       => t('Add the global groups, these groups are the groups every user is added to up login'),
    '#default_value'     => variable_get('openlucius_ldap_global_mapping', ''),
  );

  /**
   * LDAP MAPPING.
   */

  // Configuration of the dashboards
  $form['ldap_mapping'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('LDAP mapping categories'),
    '#description' => t('These are the fields in the LDAP user object'),
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE
  );

  // Initiate counter default.
  if (empty($form_state['ldap_mapping']['openlucius_ldap_mapping_counter'])) {
    $default_count = variable_get('openlucius_ldap_mapping_counter', FALSE);
    if (!$default_count) {
      $form_state['ldap_mapping']['openlucius_ldap_mapping_counter'] = 1;
    }
    else {
      $form_state['ldap_mapping']['openlucius_ldap_mapping_counter'] = $default_count;
    }
  }

  // Add counter.
  $form['ldap_mapping']['openlucius_ldap_mapping_counter'] = array(
    '#type'  => 'value',
    '#value' => $form_state['ldap_mapping']['openlucius_ldap_mapping_counter']
  );

  $i = 1;
  while ($i <= $form['ldap_mapping']['openlucius_ldap_mapping_counter']['#value']) {
    // get mapping
    $value                                                 = variable_get('openlucius_ldap_mapping_' . $i, '');
    $form['ldap_mapping']['openlucius_ldap_mapping_' . $i] = array(
      '#type'          => 'textfield',
      '#title'         => t('Mapping Category') . ' ' . $i . ':',
      '#description'   => t('These categories are used for the ldap mapping form'),
      '#default_value' => $value
    );

    $i++;
  }

  $form['ldap_mapping']['add_category'] = array(
    '#type'   => 'submit',
    '#value'  => t('Add another category'),
    '#submit' => array('openlucius_ldap_add_category')
  );

  // add remove button
  if ($form['ldap_mapping']['openlucius_ldap_mapping_counter']['#value'] > 1) {
    $form['ldap_mapping']['remove_category'] = array(
      '#type'                    => 'submit',
      '#value'                   => t('Remove last category'),
      '#submit'                  => array('openlucius_ldap_remove_category'),

      // Since we are removing a name, don't validate until later.
      '#limit_validation_errors' => array(),
    );
  }

  if (($count = variable_get('openlucius_ldap_mapping_counter', FALSE))) {

    for ($i = 1; $i <= $count; $i++) {
      $value = check_plain(variable_get('openlucius_ldap_mapping_' . $i, ''));

      $form[$value . '_mapping_ajax']                                           = array(
        '#type'  => 'fieldset',
        '#title' => t('!category mapping form', array('!category' => ucfirst($value)))
      );
      $form[$value . '_mapping_ajax']['category']                               = array(
        '#title'       => t('!category value', array('!category' => ucfirst($value))),
        '#type'        => 'textfield',
        '#description' => t('Add the term you wish to associate to a mapping rule.'),
        '#attributes'    => array(
          'class' => array('openlucius-category'),
        ),
      );
      $form[$value . '_mapping_ajax']['autocomplete']                           = array(
        '#title'             => t('Groups'),
        '#type'              => 'textfield',
        '#multiple'          => TRUE,
        '#description'       => t('Add groups (comma separated) to be associated to the category, this is an autocomplete field'),
        '#autocomplete_path' => 'autocomplete/groupname',
        '#attributes'    => array(
          'class' => array('openlucius-groups'),
        ),
      );
      $form[$value . '_mapping_ajax']['add']                                    = array(
        '#markup' => l(t('Add to ldap !category mapping', array('!category' => $value)), current_path(), array(
          'attributes' => array('class' => array('btn-xs btn-primary openlucius-ldap-add')),
        ))
      );
      $form[$value . '_mapping_ajax']['openlucius_ldap_' . $value . '_mapping_display'] = array(
        '#markup' => '<table class="ldap-mapping">
                        <thead>
                          <tr><th>' . t('Value') . '</th><th>' . t('Groups') . '</th><th></th></tr>
                        </thead>
                      </table>',
      );
      $form[$value . '_mapping_ajax']['openlucius_ldap_' . $value . '_mapping'] = array(
        '#type'          => 'hidden',
        '#default_value' => variable_get('openlucius_ldap_' . $value . '_mapping', ''),
        '#attributes'    => array(
          'class' => array('openlucius-mapping'),
        ),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Function to add categories to the dynamic form.
 */
  function openlucius_ldap_add_category($form, &$form_state) {
  $form_state['ldap_mapping']['openlucius_ldap_mapping_counter']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Function to remove categories from the dynamic form.
 */
function openlucius_ldap_remove_category($form, &$form_state) {
  // Reset variable, to prevent awkwardness;
  variable_set('openlucius_ldap_mapping_' . $form_state['ldap_mapping']['openlucius_ldap_mapping_counter'], '');
  $form_state['ldap_mapping']['openlucius_ldap_mapping_counter']--;
  $form_state['rebuild'] = TRUE;
}
