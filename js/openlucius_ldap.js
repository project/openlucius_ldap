(function ($) {
  "use strict";

  Drupal.behaviors.openlucius_ldap = {
    attach: function (context, settings) {

      /**
       * Function to check if a given value is a JSON formatted string.
       *
       * @param {String} str
       *   The string to be checked.
       *
       * @returns {boolean}
       *   Returns true when valid and false when not.
       */
      function IsJsonString(str) {
        try {
          JSON.parse(str);
        } catch (e) {
          return false;
        }

        return true;
      }

      /**
       * Function to render the mapping table.
       *
       * @param inputElement
       *   The input element for which the table should be rendered.
       */
      function renderLdapMappings(inputElement) {
        if (inputElement !== undefined) {
          var parent = inputElement.parent(),
              category  = parent.find('.openlucius-category'),
              groups    = parent.find('.openlucius-groups'),
              value  = inputElement.val(),
              target = parent.find('.ldap-mapping');

          // Check if this is valid JSON.
          if (IsJsonString(value)) {

            // Clear table.
            target.find('tbody').html('');

            // Get JSON object.
            var json = JSON.parse(value);

            for (var item in json) {

              // Check if the propery exists.
              if (json.hasOwnProperty(item) && item != '' && json[item] != '') {
                var row = '<tr><td>' + item + '</td><td>' + json[item] + '</td><td><span data-row="' + item + '" class="fa fa-times ldap-remove-row"></span></td>'
                target.append(row);

                // Reset elements for next insert.
                category.val('');
                groups.val('');
              }
            }

            // Add removal behaviour for button.
            parent.find('.ldap-remove-row').bind('click', function() {
              ldapRemoveRow($(this));
            });
          }
        }
      }

      /**
       * Remove row behaviour for buttons.
       * @param item
       *   The item for which the behaviour has to be executed.
       */
      function ldapRemoveRow(item) {

        var parent    = item.parents('.ldap-mapping').parent(),
            input     = parent.find('.openlucius-mapping'),
            value     = input.val();

        // Replace value by empty object if not JSON.
        if (!IsJsonString(value)) {
          value = {};
        }
        else {
          value = JSON.parse(value);
        }

        // Remove row.
        delete value[item.attr('data-row')];

        // Store into the hidden input.
        input.val(JSON.stringify(value)).trigger('change');
      }

      // Only trigger once.
      if (context == document) {

        var mappingElements = $('.openlucius-mapping');

        // Init all tables.
        mappingElements.each(function() {
          renderLdapMappings($(this));
        });

        // On change render the LDAP mappings.
        mappingElements.on('change', function() {

          // Render the mappings in the table.
          renderLdapMappings($(this));
        });

        // On click add item and rerender the table.
        $('.openlucius-ldap-add').on('click', function(e) {
          e.preventDefault();
          e.stopPropagation();
          var parent    = $(this).parent(),
              category  = parent.find('.openlucius-category'),
              groups    = parent.find('.openlucius-groups'),
              input     = parent.find('.openlucius-mapping'),
              value     = input.val();

          // Replace value by empty object if not JSON.
          if (!IsJsonString(value)) {
            value = {};
          }
          else {
            value = JSON.parse(value);
          }

          // Append the row.
          value[category.val()] = groups.val();

          // Store into the hidden input.
          input.val(JSON.stringify(value)).trigger('change');
        });
      }
    }
  }
})(jQuery);