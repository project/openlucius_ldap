-------------------------------------------------------------------------------

Openlucius LDAP for the openlucius distribution.

-------------------------------------------------------------------------------
DESCRIPTION:

This module adds the LDAP functionality to Openlucius.

-------------------------------------------------------------------------------

INSTALLATION:
* Put the module in your Drupal modules directory and enable it in
  admin/modules.
* Configure and use the module at admin/config/openlucius/ldap
