<?php
/**
 * @file
 * This file contains all functions and hooks required for LDAP in OpenLucius.
 */

include_once 'openlucius_ldap.features.inc';
include_once 'includes/forms.inc';
include_once 'includes/sync.inc';

/**
 * Implements hook_menu().
 */
function openlucius_ldap_menu() {
  // Initialize array.
  $items = array();

  $items['admin/config/openlucius/ldap'] = array(
    'page callback'    => 'drupal_get_form',
    'title'            => 'LDAP configurations',
    'page arguments'   => array('_openlucius_ldap_form'),
    'type'             => MENU_LOCAL_TASK,
    'access arguments' => array('openlucius configure ldap'),
  );

  // Autocomplete on node title.
  $items['autocomplete/groupname'] = array(
    'page callback'    => '_openlucius_ldap_autocomplete_group',
    'type'             => MENU_NORMAL_ITEM,
    'access arguments' => array('openlucius configure ldap'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function openlucius_ldap_permission() {
  return array(
    'openlucius configure ldap' => array(
      'title'       => t('LDAP configuration'),
      'description' => t('Grants access to the LDAP configuration form')
    ),
  );
}

/**
 * Implements hook_init().
 */
function openlucius_ldap_init() {
  $menu_item = menu_get_item();

  // Only add on config form path.
  if (!empty($menu_item['path']) && $menu_item['path'] == 'admin/config/openlucius/ldap') {

    // Add the LDAP config form css.
    drupal_add_css(drupal_get_path('module', 'openlucius_ldap') . '/css/openlucius_ldap.css');

    // Add the LDAP config form js.
    drupal_add_js(drupal_get_path('module', 'openlucius_ldap') . '/js/openlucius_ldap.js');
  }
}

/**
 * Implements hook_user_login().
 */
function openlucius_ldap_user_login(&$edit, $account) {
  // Load the account again, sometimes fields are missing.
  $account = user_load($account->uid);

  // Check if user has more than one role.
  if (count($account->roles) < 2) {

    // Add openlucius authenticated user.
    user_multiple_role_edit(array($account->uid), 'add_role', OPENLUCIUS_CORE_OPENLUCIUS_DEFAULT_ROLE);
  }

  // Skip admin for ldap process.
  if ($account->uid == 1) {
    return TRUE;
  }

  // Get sid from ldapserver.
  $sid = _openlucius_ldap_get_server();

  // Only trigger on ldap active and set.
  if ($sid) {

    // Connect to LDAP.
    $ldap = new LdapServer($sid);
    $ldap->connect();
    $ldap->bind();

    // Search the current user.
    $tmp_result = $ldap->search('', "(samaccountname=" . $account->name . ")");

    // Check if we have results.
    if ($tmp_result['count'] > 0) {

      // Initialize array for groups sync.
      $groups = array();

      // Check if global mapping is set.
      if (variable_get('openlucius_ldap_global_mapping', '') != '') {

        // Fetch the global mapping.
        $global_mapping = variable_get('openlucius_ldap_global_mapping', '');

        // Explode on comma.
        $global_entries = explode(',', $global_mapping);

        // Check if we have entries.
        if (!empty($global_entries)) {

          // Loop through entries.
          foreach ($global_entries as $entry) {

            // Check if we have a matching entry, if we do append it to groups.
            openlucius_ldap_match_group($entry, $groups);
          }
        }
      }

      // Append the groups defined in the config based on the ldap result.
      openlucius_ldap_map_groups($groups, $tmp_result);

      // Check if we have a group we should be a member of.
      if (!empty($groups)) {

        // Add user to groups.
        _openlucius_ldap_add_user_to_groups($account, $groups);
      }

      // Apply custom hook to allow other modules to hook into the login.
      openlucius_ldap_login_trigger($tmp_result);
    }
  }

  return FALSE;
}

/**
 * Function to map an LDAP result to groups.
 *
 * @param array $groups
 *   The groups this user should be in.
 * @param $ldap_entry
 *   The LDAP result for a give user.
 */
function openlucius_ldap_map_groups(array &$groups, $ldap_entry) {

  // Check if we have a value.
  if (($count = variable_get('openlucius_ldap_mapping_counter', FALSE))) {

    // Loop through the values.
    for ($i = 1; $i <= $count; $i++) {

      // Fetch the value we should read from the LDAP entry.
      $value = check_plain(variable_get('openlucius_ldap_mapping_' . $i, ''));

      // We have a value and it exists in the LDAP entry.
      if (!empty($value) && !empty($ldap_entry[0][$value]['count'])) {

        // Fetch the set mapping from the database.
        $mapping = variable_get('openlucius_ldap_' . $value . '_mapping', '');

        // Check if a mapping was made.
        if (!empty($mapping)) {

          // Decode the json values.
          $json = drupal_json_decode($mapping);

          // Loop through the values.
          for ($j = 0; $j < $ldap_entry[0][$value]['count']; $j++) {

            // Check if the entry isset.
            if (isset($json[$ldap_entry[0][$value][$j]])) {

              // Explode comma separated entries.
              $entries = explode(',', $json[$ldap_entry[0][$value][$j]]);

              // Check if we entries.
              if (!empty($entries)) {

                // Loop through the entries.
                foreach ($entries as $entry) {

                  // Check if we have a matching entry, if so add it to groups.
                  openlucius_ldap_match_group($entry, $groups);
                }
              }
            }
          }
        }
      }
    }

    // Make sure the groups are unique items.
    $groups = array_unique($groups);
  }
}

/**
 * Implements hook_form_alter().
 * Form alter for realname validation on autocomplete field.
 */
function openlucius_ldap_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'comment_node_ol_todo_form') {
    if (isset($form['field_todo_user_reference'])) {

      // Replace original by custom realname version.
      $form['field_todo_user_reference'][LANGUAGE_NONE][0]['uid']['#element_validate'][0] = '_openlucius_ldap_user_reference_autocomplete_validate';
      $form['field_todo_user_reference'][LANGUAGE_NONE][0]['uid']['#value_callback']      = '_openlucius_ldap_user_reference_autocomplete_value';
    }
  }
}

/**
 * Function to obtain LDAP server if module is available and active.
 */
function _openlucius_ldap_get_server() {

  // We don't have the module so return false.
  if (!module_exists('ldap_servers')) {
    return FALSE;
  }

  // Return sid required for binding.
  else {
    // Fetch default (for older installs).
    $sid = db_query('SELECT sid FROM {ldap_servers} WHERE 1 LIMIT 0,1')->fetchField();

    // The newer installs have their sids in a variable.
    if (empty($sid)) {
      $config = variable_get("ldap_authentication_conf");
      $keys   = array_keys($config['sids']);
      $sid    = !empty($config['sids']) ? $config['sids'][$keys[0]] : FALSE;
    }

    return $sid;
  }
}

/**
 * Function to add a user to a list of groups (and their parents).
 *
 * @param \stdClass $user
 *   The user to be added to groups (user object)
 * @param array $groups
 *   The groups to add the user to.
 */
function _openlucius_ldap_add_user_to_groups($user, $groups) {

  // If empty add all groups.
  if (empty($user->field_groups)) {

    // Add user to group.
    foreach ($groups as $group) {
      $user->field_groups[LANGUAGE_NONE][] = array('nid' => $group);
    }
  }
  else {
    // Get user groups array.
    $user_groups = array();
    foreach ($user->field_groups[LANGUAGE_NONE] as $group) {
      $user_groups[] = $group['nid'];
    }

    // Remove doubles if any.
    $user_groups = array_unique($user_groups);

    // Loop through groups.
    foreach ($groups as $group) {

      // Check if the user is already in this group.
      if (!in_array($group, $user_groups)) {
        $user->field_groups[LANGUAGE_NONE][] = array('nid' => $group);
      }
    }
  }

  // Save the user, done.
  user_save($user);
}

/**
 * Autocomplete function for groups.
 */
function _openlucius_ldap_autocomplete_group($string) {
  // Import for safe cleaning.
  require_once drupal_get_path('module', 'ctools') . '/includes/cleanstring.inc';

  $matches = array();

  // Required for mutliple entries.
  $items = array_map('trim', explode(',', $string));

  // Get last as this is the one we need.
  $last_item = array_pop($items);

  // Items are comma separated so add comma.
  $prefix = implode(', ', $items);

  $return = db_query("
    SELECT n.nid, n.title
    FROM {node} n
    WHERE lower(n.title) like '%" . ctools_cleanstring(strtolower($last_item)) . "%'
    AND n.type = 'ol_group'
    LIMIT 0, 10")->fetchAll();

  // Add matches to $matches.
  foreach ($return as $row) {

    // Build string.
    $item = $row->title . '[nid:' . $row->nid . ']';

    // Add with other items.
    $value = !empty($prefix) ? $prefix . ', ' . $row->title . ', ' : $row->title . ', ';
    $nids  = !empty($prefix) ? $prefix . ', ' . $item . ', ' : $item . ', ';

    // Add to matches.
    $matches[$nids] = check_plain($value);
  }

  // Return for JS.
  drupal_json_output($matches);
  drupal_exit();
}

/**
 * User reference value callback for realname.
 */
function _openlucius_ldap_user_reference_autocomplete_value($element, $input = FALSE, $form_state) {
  if ($input === FALSE) {
    // We're building the displayed 'default value': expand the raw uid into
    // "user name [uid:n]".
    $uid = $element['#default_value'];

    if (!empty($uid)) {
      $result = db_query('
        SELECT
        CASE
          WHEN LENGTH(r.realname) = 0
          OR r.realname IS NULL
          THEN u.name
          ELSE r.realname
        END AS name
        FROM {users} u
        INNER JOIN {realname} r
        ON u.uid = r.uid
        WHERE r.uid = :uid
        LIMIT 0, 1', array(':uid' => $uid));

      // @FIXME If no result (user doesn't exist).
      $value = $result->fetchField();
      $value .= ' [uid:' . $uid . ']';
      return $value;
    }
  }
}

/**
 * Validation callback for a user_reference autocomplete element.
 * This function replaces the original user_reference_autocomplete_validate().
 * @see user_reference_autocomplete_validate()
 */
function _openlucius_ldap_user_reference_autocomplete_validate($element, &$form_state, $form) {
  $field    = field_widget_field($element, $form_state);
  $instance = field_widget_instance($element, $form_state);

  $value = $element['#value'];
  $uid   = NULL;

  if (!empty($value)) {
    // Check whether we have an explicit "[uid:n]" input.
    preg_match('/^(?:\s*|(.*) )?\[\s*uid\s*:\s*(\d+)\s*\]$/', $value, $matches);
    if (!empty($matches)) {
      // Explicit uid. Check that the 'name' part matches the actual name for
      // the uid.
      list(, $name, $uid) = $matches;
      if (!empty($name)) {
        $names     = _user_reference_get_user_names(array($uid));
        $realnames = _openlucius_ldap_user_reference_get_real_names(array($uid));
        if ($name != $names[$uid] && $name != $realnames[$uid]) {
          form_error($element, t('%name: name mismatch. Please check your selection.', array('%name' => $instance['label'])));
        }
      }
    }
    else {
      // No explicit uid (the submitted value was not populated by autocomplete
      // selection). Get the uid of a referencable user from the entered name.
      $options    = array(
        'string' => $value,
        'match'  => 'equals',
        'limit'  => 1,
      );
      $references = user_reference_potential_references($field, $options);
      if ($references) {
        // @todo The best thing would be to present the user with an
        // additional form, allowing the user to choose between valid
        // candidates with the same name. ATM, we pick the first
        // matching candidate...
        $uid = key($references);
      }
      else {
        form_error($element, t('%name: found no valid user with that name.', array('%name' => $instance['label'])));
      }
    }
  }

  // Set the element's value as the user id that was extracted from the entered
  // input.
  form_set_value($element, $uid, $form_state);
}

/**
 * Function to fetch realname values and their uids.
 *
 * @param array $uids
 *   An array containing the user id's.
 */
function _openlucius_ldap_user_reference_get_real_names(array $uids) {

  $query = db_select('realname', 'r')
    ->fields('r', array('uid', 'realname'))
    ->condition('r.uid', $uids);
  return $query->execute()->fetchAllKeyed(0, 1);
}

/**
 * Custom hook for ldap search query.
 */
function openlucius_ldap_get_ldap_search_query() {
  $variables = array('(&(objectcategory=person)(objectclass=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))');

  // Allow modules to alter the variables.
  if (count(module_implements('openlucius_ldap_get_ldap_search_query')) > 0) {

    // Call modules that implement the hook, and let them change $variables.
    $variables = module_invoke_all('openlucius_ldap_get_ldap_search_query', $variables);
  }
  return $variables;
}

/**
 * LDAP drush functionality below this line.
 */

/**
 * Custom batch operation for importing users.
 */
function _openlucius_ldap_sync_setup_import_batch() {
  // Get sid from LDAP server.
  $sid = _openlucius_ldap_get_server();
  $i   = 0;

  // Array for storing operations.
  $operations = array();

  // Check if we have a server.
  if ($sid != FALSE) {

    // Start up ldap connection.
    $ldap = new LdapServer($sid);
    $ldap->connect();
    $ldap->bind();
    $ldap_query = openlucius_ldap_get_ldap_search_query();

    // Find all users using a wildcard.
    $variables['results'] = $ldap->search('', $ldap_query[0], array(), 0, 10000);

    // Loop through users.
    foreach ($variables['results'] as $user) {

      // Skip empty.
      if ($user['samaccountname'][0] == '' || empty($user['samaccountname'])) {
        continue;
      }

      // Skip service accounts, usually detectable in [distinguishedname].
      if (stristr($user['distinguishedname'][0], 'service')) {
        continue;
      }

      // Load by mail && load by name.
      $account  = user_load_by_mail($user['mail'][0]);
      $account2 = user_load_by_name($user['samaccountname'][0]);

      // The account exists but it was probably created before LDAP could
      // for example manually.
      if (empty($account2) && !empty($account)) {

        // Store message.
        watchdog('ldap_merge', drupal_json_encode(array('uid' => $account->uid, 'ldap' => $user, 'old_data' => $account)));

        // Set realname to the current name so the display doesn't change..
        $account->realname = $account->name;

        // Set the name to the samaccountname so LDAP is used for login.
        $account->name = $user['samaccountname'][0];

        // Settings where merged and save it.
        user_save($account);
        continue;
      }
      // We have an account but the mail entry does not match the one in the
      // Database.
      elseif (!empty($account2) && empty($account)) {
        watchdog('ldap_mail_sync', drupal_json_encode(array('uid' => $account2->uid, 'old_data' => $account2, 'new_mail' => $user['mail'][0])));
        $account2->mail = $user['mail'][0];
        user_save($account2);
        continue;
      }
      // Both exist we have a duplicate account.
      elseif (!empty($account) && !empty($account2) && $account->uid != $account2->uid) {
        watchdog('ldap_duplicate', drupal_json_encode(array('first' => $account, 'second' => $account2)));
        continue;
      }
      // This is just an existing account skip.
      elseif (!empty($account) && !empty($account2) && $account->uid == $account2->uid) {
        continue;
      }
      // This is a new account hurray.
      else {
        $i++;

        // Add new operation to the batch.
        $operations[] = array(
          '_openlucius_ldap_create_new_user',
          array(
            $user,
            'details' => t('(Importing user @user)', array(
              '@user' => $i,
            )),
          ),
        );
      }
    }
  }

  // Show the users that something is happening.
  print 'New users: ' . count($operations) . "\n";

  // Put all that information into our batch array.
  $batch = array(
    'operations'    => $operations,
    'title'         => t('User Import'),
    'init_message'  => t('Initializing'),
    'error_message' => t('An error occurred'),
    'finished'      => '_openlucius_ldap_batch_finished',
  );

  // Get the batch process all ready.
  batch_set($batch);
  $batch =& batch_get();

  // Because we are doing this on the back-end, we set progressive to false.
  $batch['progressive'] = FALSE;

  // Start processing the batch operations.
  drush_backend_batch_process();
}

/**
 * Function to add new user.
 */
function _openlucius_ldap_create_new_user($user, $operation_details, &$context) {
  // Get display name if any.
  $display_name = isset($user['cn'][0]) ? $user['cn'][0] : $user['samaccountname'][0];

  // Initialize array.
  $fields = array(
    // Add username.
    'name'   => $user['samaccountname'][0],
    // Add temporary password, LDAP will override this on first login.
    'pass'   => user_password(15),
    // Set user to active.
    'status' => 1,
    // Set activation type.
    'init'   => 'email address',
    // Add default role.
    'roles'  => array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      OPENLUCIUS_CORE_OPENLUCIUS_DEFAULT_ROLE => 'openlucius authenticated user',
    ),
  );

  // Check if user has mail.
  if (isset($user['mail'][0])) {
    $fields['mail'] = $user['mail'][0];
  }
  else {
    // Fetch domain from server.
    $domain = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'openlucius.nl';

    // Allow modules to alter the default domain.
    drupal_alter('openlucius_ldap_default_domain', $domain);

    // Set default mail address.
    $fields['mail'] = $user['samaccountname'][0] . '@' . $domain;
  }

  // Initial email.
  $fields['init'] = $fields['mail'];

  // Allow other modules to alter the $fields before saving.
  // The LDAP result passed along for replacing the defaults by other values.
  $fields = openlucius_ldap_pre_create_user($fields, $user);

  // The first parameter is left blank so a new user is created.
  $account = user_save('', $fields);

  // Check if global mapping is set.
  $global_mapping = variable_get('openlucius_ldap_global_mapping', '');
  if ($global_mapping != '' && !empty($account)) {

    // Explode and build array.
    $global_mapping = explode(',', $global_mapping);
    $groups = array();
    foreach ($global_mapping as $item) {
      openlucius_ldap_match_group($item, $groups);
    }

    // Add user to default groups.
    _openlucius_ldap_add_user_to_groups($account, $groups);
  }

  // Use the drupal entity wrapper to fill in non standard fields
  // saves the hassle of using [LANGUAGE_NONE][0][value] etc.
  $user_wrapper = entity_metadata_wrapper('user', $account);
  $user_wrapper->field_user_display_name->set($display_name);

  // Allow other modules to alter the user_wrapper before saving.
  drupal_alter('openlucius_ldap_after_create_new_user', $user_wrapper, $user);

  // Save the user.
  $user_wrapper->save();

  // Allow other modules to react after a user has been created.
  openlucius_ldap_post_create_user($user_wrapper, $fields);

  // Will show which user is being processed.
  $context['message'] = $operation_details;
}

/**
 * Batch is finished.
 */
function _openlucius_ldap_batch_finished($success, $results, $operations) {
  // Let the user know we have finished!
  print t('Finished batch!') . "\n";
}

/**
 * Custom hook for ldap login triggers.
 *
 * @param array $result
 *   The result obtained from LDAP.
 *
 * @return mixed
 *   Returns the value after other modules have mingled with it.
 */
function openlucius_ldap_login_trigger($result) {
  if (count(module_implements('openlucius_ldap_login_trigger')) > 0) {

    // Expose the LDAP result to other modules to react upon.
    $result = module_invoke_all('openlucius_ldap_login_trigger', $result);
  }

  return $result;
}

/**
 * Custom hook to allow other modules to alter the fields.
 *
 * @param array $fields
 *   The user fields array to be altered.
 * @param array $data
 *   The LDAP data of a user.
 *
 * @return array
 *   The fields array to be used for user creation.
 */
function openlucius_ldap_pre_create_user($fields, $data) {
  if (count(module_implements('openlucius_ldap_pre_create_user')) > 0) {

    // Expose the LDAP result to other modules to react upon.
    $fields = module_invoke_all('openlucius_ldap_pre_create_user', $fields, $data);
  }

  return $fields;
}

/**
 * Custom hook to allow other modules to alter the user_wrapper after saving.
 *
 * @param \EntityMetadataWrapper $user_wrapper
 *   The EntityMetadataWrapper to be altered or used.
 * @param array $data
 *   The LDAP data which was used to create the entity.
 *
 * @return \EntityMetadataWrapper
 *   The EntityMetadataWrapper after usage.
 */
function openlucius_ldap_post_create_user(\EntityMetadataWrapper $user_wrapper, $data) {
  if (count(module_implements('openlucius_ldap_post_create_user')) > 0) {

    // Expose the LDAP result to other modules to react upon.
    $user_wrapper = module_invoke_all('openlucius_ldap_post_create_user', $user_wrapper, $data);
  }

  return $user_wrapper;
}

/**
 * Implements hook_openlucius_core_config_places_alter().
 */
function openlucius_ldap_openlucius_core_config_places_alter(&$places) {
  $places[] = 'admin/config/openlucius/ldap';
}

/**
 * Function to match a nid, check if the group exists and add it to the list.
 * @param string $entry
 *   The entry to be matched.
 * @param array $groups
 *   The array where the node id can be added.
 */
function openlucius_ldap_match_group($entry, array &$groups) {

  // Get node id from [nid:number]
  preg_match('#\[nid\:(.*?)\]#', check_plain($entry), $matches);

  // Check if we have a numeric value.
  if (is_numeric($matches[1])) {

    // verify that node exists and is published.
    $exists = db_query("SELECT nid
                            FROM {node}
                            WHERE nid = :nid
                            AND type = 'ol_group'
                            AND status = 1", array(':nid' => $matches[1])) -> fetchField();

    // If the node exists add it to the groups array.
    if (!empty($exists)) {
      $groups[] = $exists;
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function openlucius_ldap_form_user_register_form_alter(&$form, &$form_state, $form_id) {
  // Hide LDAP options.
  if (!empty($form['ldap_user_fields'])) {
    $form['ldap_user_fields']['#access'] = FALSE;
  }
}
